<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->date('start_time');
            $table->bigInteger('teacher_id')->unsigned()->nullable();
            $table->foreign('teacher_id')
                ->references('id')->on('teachers')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('classes', function (Blueprint $table) {
            $table->dropForeign('classes_teacher_id_foreign');
        });

        Schema::dropIfExists('classes');
    }
}
