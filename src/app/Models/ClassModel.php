<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassModel extends Model
{

    protected $table = 'classes';
    protected $fillable = [
        'name',
        'start_time',
        'teacher_id'
    ];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function students() {

        return $this->belongsToMany(
            Student::class,
            'class_student',
            'class_id',
            'student_id'
        );
    }
}
