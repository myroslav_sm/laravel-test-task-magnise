<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'age',
        'class_id',
    ];

    public function classes() {
        return $this->belongsToMany(
            ClassModel::class,
            'class_student',
            'student_id',
            'class_id'
        );

    }
}
