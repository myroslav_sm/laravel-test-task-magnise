<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ClassModel;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $classToSave = $request->validate([
            'name' => 'required|string|max:255',
            'start_time' => 'required|date',
            'teacher_id' => 'sometimes|integer|exists:teachers,id',
        ]);

        $class = new ClassModel($classToSave);

        $class->save();

        return response()->json($class, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $class = ClassModel::with('teacher')->where('id', $id)->get();

        if (!$class) {
            return response()->json(['message' => 'Class not Found'], 404);
        }

        return response()->json($class, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $class = ClassModel::find($id);

        if (!$class) {
            return response()->json(['message' => 'Class not Found'], 404);
        }

        $classToUpdate = $request->validate([
            'name' => 'string|max:255',
            'date' => 'date',
            'teacher_id' => 'sometimes|integer|exists:teachers,id',
        ]);

        $class->update($classToUpdate);

        return response()->json($class, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $class = ClassModel::find($id);

        if (!$class) {
            return response()->json(['message' => 'Class not Found'], 404);
        }

        $class->delete($id);

        return response()->json(['message' => 'Class deleted'], 200);
    }


    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getStudentsByClassId ($id) {
        $class = ClassModel::find($id);

        if (!$class) {
            return response()->json(['message' => 'Class not Found'], 404);
        }

        return response()->json($class->students, 200);
    }
}
