<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Student;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $studentToSave = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'age' => 'required|integer',
        ]);

        $classId = $request->validate([
            'class_id' => 'sometimes|integer|exists:classes,id',
        ]);

        $student = DB::transaction(function () use ($studentToSave, $classId) {
            $_student = new Student($studentToSave);

            $_student->save();

            if (isset($classId)) {
                $_student->classes()->sync([$classId]);
            }

            return $_student;
        });



        return response()->json($student, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::with('classes')->where('id', $id)->get();

        if (!$student) {
            return response()->json(['message' => 'Student not Found'], 404);
        }

        return response()->json($student, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);

        if (!$student) {
            return response()->json(['message' => 'Student not Found'], 404);
        }

        $studentToUpdate = $request->validate([
            'first_name' => 'string|max:255',
            'last_name' => 'string|max:255',
            'age' => 'integer'
        ]);

        $classId = $request->validate([
            'class_id' => 'sometimes|integer|exists:classes,id',
        ]);

        $updatedStudent = DB::transaction(function () use ($studentToUpdate, $student, $classId) {
            $student->update($studentToUpdate);

            if (isset($classId)) {
                $student->classes()->sync([$classId]);
            }

            return $student;
        });

        return response()->json($updatedStudent, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);

        if (!$student) {
            return response()->json(['message' => 'Student not Found'], 404);
        }

        $student->delete($id);

        return response()->json(['message' => 'Student deleted'], 200);
    }

    /**
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getClassesByStudentId ($id) {
        $student = Student::find($id);

        if (!$student) {
            return response()->json(['message' => 'Student not Found'], 404);
        }

        return response()->json($student->classes, 200);
    }
}
